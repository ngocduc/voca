/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import CardFlip from 'components/Card';
import styled from 'styled-components';
import { Button } from 'semantic-ui-react';

const Div = styled.div`
  position: absolute;
  height: 200px;
  width: 100%;
  background: #fff;
  border-radius: 5px;
  box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.2);
  padding: 20px 70px 20px 30px;
  position: fixed;
  bottom: 10px;
  left: 0px;
  .next,
  .back {
    height: 41px;
    width: 10%;
    color: #fff;
    font-size: 15px;
    background: #fff;
    color: #00c5f9;
    border-radius: 5px;
    margin-top: 5px;
    margin-left: 20px;
    text-align: center;
    border: 1px solid #00c5f9;
    border-radius: 30px 30px 30px 30px;
  }
  .next {
    float: right;
    color: #fff;
  }
`;
const length = 10;
const data = Array(length)
  .fill(null)
  .map((e, index) => (
    <CardFlip
      key={Math.random()}
      meansVN={`từ vựng - ${index}`}
      spell="/və'kæbjuləri/"
      definition="the body of words used in a particular language."
      example="the term became part of business vocabulary "
      meansEN="Vocabulary"
      imgUrl="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvcu8F8jG55JyC19hveho5OXExey5s2SCRJoLJ9qCJ34ZLs7j8gQ"
    />
  ));
export default class Learn extends React.Component {
  constructor() {
    super();
    this.state = {
      item: 1,
    };
  }

  // componentDidUpdate(state) {
  //   console.log('state', state);
  //   setTimeout(() => {
  //     const delayedClassNames = this.refs.noDelayed.className;
  //     this.setState({
  //         render: true,
  //         classNames: delayedClassNames
  //     });
  // }, 1000);
  // }

  handleSwitchCard(up) {
    this.setState(preState => {
      const tmp = up ? 1 : -1;
      const num = (preState.item + tmp) % length;
      const item = num >= 0 ? num : 0;
      return {
        item,
      };
    });
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>intermediate</title>
          <meta
            name="description"
            content="Feature page of React.js Boilerplate application"
          />
        </Helmet>
        {data[this.state.item]}
        <Div>
          <Button color="green">Green</Button>
          {/* <Button className="next" conClick={() => this.handleSwitchCard(1)}>
            Tiếp theo
          </Button> */}
          {/* <Button className="back" onClick={() => this.handleSwitchCard(0)}>
            Quay lại
          </Button> */}
        </Div>
      </div>
    );
  }
}
