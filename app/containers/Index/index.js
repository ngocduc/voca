import PropTypes from 'prop-types';
import React from 'react';
import {
  Container,
  Grid,
  Header,
  List,
  Segment,
  Card,
  Image,
  Button,
} from 'semantic-ui-react';
import DesktopContainer from 'components/Destop';
import MobileContainer from 'components/Mobile';
import img from 'images/bg1.jpg';
const CardExampleImageCard = () => (
  <Card>
    <Image src={img} />
    <Card.Content>
      <Card.Header>name</Card.Header>
      <Card.Meta>national</Card.Meta>
      <Card.Description>Description</Card.Description>
    </Card.Content>
    <Card.Content extra>
      <div className="ui two buttons">
        <Button basic color="green">
          menber
        </Button>
        <Button basic color="red">
          time
        </Button>
      </div>
    </Card.Content>
  </Card>
);
export const ResponsiveContainer = ({ children }) => (
  <div>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </div>
);

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
};

const HomepageLayout = () => (
  <ResponsiveContainer>
    <Segment style={{ padding: '8em 0em' }} vertical>
      <Grid container columns={2} doubling>
        <Grid.Column width={5}>
          <Segment>{CardExampleImageCard()}</Segment>
        </Grid.Column>
        <Grid.Column width={11}>
          <Grid container columns={3} doubling>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
            <Grid.Column>
              <Segment>{CardExampleImageCard()}</Segment>
            </Grid.Column>
          </Grid>
        </Grid.Column>
      </Grid>
    </Segment>
    <Segment inverted vertical style={{ padding: '5em 0em' }}>
      <Container>
        <Grid divided inverted stackable>
          <Grid.Row>
            <Grid.Column width={3}>
              <Header inverted as="h4" content="About" />
              <List link inverted>
                <List.Item as="a">Sitemap</List.Item>
                <List.Item as="a">Contact Us</List.Item>
                <List.Item as="a">Religious Ceremonies</List.Item>
                <List.Item as="a">Gazebo Plans</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={3}>
              <Header inverted as="h4" content="Services" />
              <List link inverted>
                <List.Item as="a">Banana Pre-Order</List.Item>
                <List.Item as="a">DNA FAQ</List.Item>
                <List.Item as="a">How To Access</List.Item>
                <List.Item as="a">Favorite X-Men</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={7}>
              <Header as="h4" inverted>
                Footer Header
              </Header>
              <p>
                Extra space for a call to action inside the footer that could
                help re-engage users.
              </p>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </Segment>
  </ResponsiveContainer>
);
export default HomepageLayout;
