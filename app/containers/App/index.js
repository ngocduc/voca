/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import FeaturePage from 'containers/FeaturePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Learning from 'containers/Learning/Loadable';
import Index from 'containers/Index/Loadable';
import ErrorBoundary from 'components/ErrorBoundary';
// import Header from 'components/Header';
// import Footer from 'components/Footer';

// import GlobalStyle from '../../global-styles';

const AppWrapper = styled.div`
  height: 100%;
`;

export default function App() {
  return (
    <ErrorBoundary>
      <AppWrapper>
        <Helmet
          titleTemplate="%s - React.js Boilerplate"
          defaultTitle="React.js Boilerplate"
        >
          <meta
            name="description"
            content="A React.js Boilerplate application"
          />
        </Helmet>
        <Switch>
          <Route exact path="/" component={FeaturePage} />
          <Route path="/index" component={Index} />
          <Route path="/learning" component={Learning} />
          <Route path="" component={NotFoundPage} />
        </Switch>
      </AppWrapper>
    </ErrorBoundary>
  );
}
