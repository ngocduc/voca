import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Grid, Header, Segment, Icon } from 'semantic-ui-react';

const LoginForm = ({ login, showPopup, closePopup }) => {
  const handle = (action, e) => {
    closePopup();
    showPopup(action, e);
  };
  return (
    <div className="login-form">
      <Segment vertical>
        <Grid celled verticalAlign="middle">
          <Grid.Row className="dsf" textAlign="left">
            <Grid.Column>
              <Header as="h3" color="teal">
                {`Chào mừng đến website`}
              </Header>
              {login ? (
                <p>Điền đầy đủ thông tin bên dưới để đăng nhập</p>
              ) : (
                <p>Điền đầy đủ thông tin bên dưới để đăng ký</p>
              )}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <Form size="large">
                <Segment vertical>
                  <Form.Input
                    fluid
                    icon="user"
                    iconPosition="left"
                    placeholder="E-mail address"
                  />
                  <Form.Input
                    fluid
                    icon="lock"
                    iconPosition="left"
                    placeholder="Password"
                    type="password"
                  />
                </Segment>
              </Form>
              <Button size="large" color="teal">
                {login ? <p> Đăng nhập</p> : <p>Đăng ký</p>}
              </Button>
              <Button size="large" color="facebook">
                <Icon name="facebook" /> Đăng nhập bằng Facebook
              </Button>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column textAlign="center">
              {login ? (
                <span>
                  Need an account?{' '}
                  <Button onClick={e => handle('sign-up', e)}>Sign up</Button>
                </span>
              ) : (
                <p>
                  <span>
                    Bạn đã có tài khoản{' '}
                    <Button onClick={e => handle('login', e)}>đăng nhập</Button>
                    ngay.
                  </span>
                </p>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </div>
  );
};

LoginForm.propTypes = {
  login: PropTypes.any,
  showPopup: PropTypes.func,
  closePopup: PropTypes.func,
};

export default LoginForm;
