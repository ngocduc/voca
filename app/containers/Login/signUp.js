import React from 'react';
import { Button, Form, Grid, Header, Segment, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const LoginForm = () => (
  <div className="login-form">
    {/*
      Heads up! The styles below are necessary for the correct render of this example.
      You can do same with CSS, the main idea is that all the elements up to the `Grid`
      below must have a height of 100%.
    */}
    <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style>
    <Segment style={{ padding: '15% 25%' }} vertical>
      <Grid container verticalAlign="middle">
        <Grid.Row textAlign="center">
          <Grid.Column>
            <Header as="h2" color="teal" textAlign="center">
              Welcom to Vocab
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row divided>
          <Grid.Column width={10}>
            <Form size="large">
              <Segment stacked>
                <Form.Input
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="E-mail address"
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                />

                <Button color="teal" fluid size="large">
                  Sign in
                </Button>
              </Segment>
            </Form>
          </Grid.Column>
          <Grid.Column width={5}>
            <Segment vertical>
              <Button color="facebook">
                <Icon name="facebook" /> Facebook
              </Button>
            </Segment>
            <Segment vertical>
              <Button color="google plus">
                <Icon name="google plus" /> Google Plus
              </Button>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column textAlign="center">
            <span>You have account? </span>
            <Link to="/sign-up">Login</Link>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  </div>
);

export default LoginForm;
