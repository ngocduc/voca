/**
 * Asynchronously loads the component for FeaturePage
 */
import loadable from 'loadable-components';

// export default loadable(() => import('./index'));

export default loadable(() => import('./index'));
// export const SignUp = loadable(() => import('./signUp'));
