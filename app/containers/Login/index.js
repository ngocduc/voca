import React from 'react';
import PropTypes from 'prop-types';
import {
  Button,
  Form,
  Grid,
  Header,
  Segment,
  Icon,
  Message,
  Dimmer,
  Loader,
} from 'semantic-ui-react';
import {
  login as doLogin,
  setToken,
  validateEmail,
  isLoggedIn,
} from 'services/common/auth';
import { Redirect } from 'react-router-dom';

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { load: false };
    this.handleToogle = this.handleToogle.bind(this);
    this.handleOnchange = this.handleOnchange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    if (this.props.type) {
      this.setState({
        type: this.props.type,
      });
    }
  }

  handleOnchange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
      errors: '',
    });
  }

  opentLoading() {
    this.setState({ load: true });
  }

  closeLoading() {
    this.setState({ load: false });
  }

  async handleSubmit() {
    const { type } = this.state;
    const { username = '', password = '' } = this.state;
    let errors;
    if (!username || !password) {
      errors = 'plesase fill all field';
    } else if (!validateEmail(username)) {
      errors = 'email is incorect';
    }
    // else if (password.length < 6) {
    //   errors = 'password have at leat 6 charactor';
    // }
    if (!errors) {
      if (type === 'login') {
        try {
          this.opentLoading();
          const token = await doLogin(username, password);
          setToken(token);
          this.closeLoading();
        } catch (err) {
          this.closeLoading();
          this.setState({ errors: err });
          setTimeout(() => this.setState({ errors: '' }), 5000);
        }
      }
    } else {
      this.setState({ errors });
    }
  }

  handleToogle(action) {
    this.setState({ type: action });
  }

  render() {
    if (isLoggedIn()) {
      return <Redirect to="/" />;
    }
    const { errors, type, load } = this.state;
    return (
      <div className="login-form">
        <Dimmer active={load}>
          <Loader size="huge"> Loading</Loader>
        </Dimmer>
        <Segment vertical>
          <Grid celled verticalAlign="middle">
            <Grid.Row className="dsf" textAlign="left">
              <Grid.Column>
                <Header as="h3" color="teal">
                  {`Chào mừng đến website`}
                </Header>
                {type === 'login' && (
                  <p>Điền đầy đủ thông tin bên dưới để đăng nhập</p>
                )}
                {type === 'sign-in' && (
                  <p>Điền đầy đủ thông tin bên dưới để đăng ký</p>
                )}
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={16}>
                <Form size="large">
                  <Segment vertical>
                    <Form.Input
                      name="username"
                      onChange={this.handleOnchange}
                      fluid
                      icon="user"
                      iconPosition="left"
                      placeholder="E-mail address"
                    />
                    <Form.Input
                      name="password"
                      onChange={this.handleOnchange}
                      fluid
                      icon="lock"
                      iconPosition="left"
                      placeholder="Password"
                      type="password"
                    />
                  </Segment>
                </Form>
                {errors && (
                  <Segment vertical>
                    <Message color="red">
                      <Message.Header>{errors}</Message.Header>
                    </Message>
                  </Segment>
                )}
                <Button onClick={this.handleSubmit} size="large" color="teal">
                  {type === 'login' && <p> Đăng nhập</p>}
                  {type === 'sign-in' && <p>Đăng ký</p>}
                </Button>
                <Button size="large" color="facebook">
                  <Icon name="facebook" /> Đăng nhập bằng Facebook
                </Button>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column textAlign="center">
                {type === 'login' && (
                  <span>
                    Need an account?{' '}
                    <Button onClick={() => this.handleToogle('sign-in')}>
                      Sign up
                    </Button>
                  </span>
                )}
                {type === 'sign-in' && (
                  <span>
                    Bạn đã có tài khoản{' '}
                    <Button onClick={() => this.handleToogle('login')}>
                      đăng nhập
                    </Button>
                    ngay.
                  </span>
                )}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Segment>
      </div>
    );
  }
}
Login.propTypes = {
  type: PropTypes.any,
};

export default Login;
