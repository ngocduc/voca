/*
 * FeaturePage
 *{
    "topic": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "name": "job",
                "description": "work for which you receive regular payment",
                "description_vn": "Làm việc để nhận tiền",
                "photo": "job.png",
                "deleted_at": null,
                "created_at": null,
                "updated_at": null
            }
        ],
        "first_page_url": "http://bf3ee26b.ngrok.io/api/topic?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://bf3ee26b.ngrok.io/api/topic?page=1",
        "next_page_url": null,
        "path": "http://bf3ee26b.ngrok.io/api/topic",
        "per_page": 10,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import { Switch, Route, Redirect } from 'react-router-dom';
import { isLoggedIn, setToken } from 'services/common/auth';
// import Index from 'containers/Learning/Loadable';
import Topic from 'containers/Topic/Loadable';
export default class FeaturePage extends React.Component {
  // Since state and props are static,
  // there's no need to re-render this component
  shouldComponentUpdate() {
    return false;
  }

  render() {
    if (!isLoggedIn()) {
      setToken();
      return <Redirect to="/index" />;
    }
    return (
      <div>
        <Helmet>
          <title>Feature Page</title>
          <meta
            name="description"
            content="Feature page of React.js Boilerplate application"
          />
        </Helmet>
        <Switch>
          <Route exact path="/" component={Topic} />
          {/* <Route path="/topic" component={Topic} /> */}
        </Switch>
      </div>
    );
  }
}
