// import PropTypes from 'prop-types';
import React from 'react';
import {
  Image,
  Grid,
  Header,
  Segment,
  List,
  Button,
  Card,
  Dimmer,
  Loader,
} from 'semantic-ui-react';
import { ResponsiveContainer } from 'containers/Index';
import { PieChart } from 'react-easy-chart';
import img from 'images/bg1.jpg';
import { ROOT } from 'services/common/config';
import { getTopic } from 'services/common/topic';
// {
//   "data": {
//       "message": "success",
//       "result": {
//           "current_page": 1,
//           "data": [
//               {
//                   "id": 1,
//                   "name": "architecture",
//                   "description": "Xây dựng",
//                   "description_vn": "architecture",
//                   "photo": "job.jpg",
//                   "deleted_at": null,
//                   "created_at": null,
//                   "updated_at": null,
//                   "category_id": 1
//               }
//           ],
//           "first_page_url": "http://d5652948.ngrok.io/api/topic/topicByCategory?page=1",
//           "from": 1,
//           "last_page": 1,
//           "last_page_url": "http://d5652948.ngrok.io/api/topic/topicByCategory?page=1",
//           "next_page_url": null,
//           "path": "http://d5652948.ngrok.io/api/topic/topicByCategory",
//           "per_page": 10,
//           "prev_page_url": null,
//           "to": 1,
//           "total": 1
//       }
//   }
// }
const CardExampleCard = () => (
  <Card>
    <Card.Content>
      <Image
        floated="right"
        style={{
          height: '60px',
          width: '60px',
        }}
        size="tiny"
        circular
        src={img}
      />
      <Card.Header>Steve Sanders</Card.Header>
      <Card.Meta>Friends of Elliot</Card.Meta>
      <Card.Description>
        Steve wants to add you to the group <strong>best friends</strong>
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      <div className="ui two buttons">
        <Button basic color="green">
          Approve
        </Button>
        <Button basic color="red">
          Decline
        </Button>
      </div>
    </Card.Content>
  </Card>
);
class HomepageLayout extends React.Component {
  constructor() {
    super();
    this.state = { load: false, infor: {} };
  }

  componentWillMount() {
    this.opentLoading();
    getTopic(1).then(data => {
      const res = data.result.data[0];
      this.setState({
        infor: {
          description: res.description,
          description_vn: res.description_vn,
          name: res.name,
          photo: res.photo,
        },
      });
      this.closeLoading();
    });
  }

  opentLoading() {
    this.setState({ load: true });
  }

  closeLoading() {
    this.setState({ load: false });
  }

  render() {
    const { load, infor } = this.state;
    return (
      <ResponsiveContainer>
        <Dimmer active={load}>
          <Loader size="huge"> Loading</Loader>
        </Dimmer>
        <Segment vertical style={{ padding: '8em 0em' }}>
          <Grid container verticalAlign="middle" divided="vertically">
            <Grid.Row>
              <Grid.Column width={1} />
              <Grid.Column width={9}>
                <Segment textAlign="center">
                  <Header>
                    {infor.description}
                    (Anh-Việt): ? từ
                  </Header>
                  <Grid container verticalAlign="middle" divided="vertically">
                    <Grid.Row>
                      <Grid.Column width={6}>
                        <Segment vertical textAlign="center">
                          <Image src={`${ROOT}/images/topics/${infor.photo}`} />
                        </Segment>
                      </Grid.Column>
                      <Grid.Column width={10}>
                        <Grid
                          container
                          verticalAlign="middle"
                          divided="vertically"
                        >
                          <Grid.Column width={8}>
                            <Segment vertical textAlign="left">
                              <Header>20 từ</Header>
                              <List>
                                <List.Item>
                                  <List.Icon color="green" name="marker" />
                                  <List.Content> đã thuộc</List.Content>
                                </List.Item>
                                <List.Item>
                                  <List.Icon color="orange" name="marker" />
                                  <List.Content>đang học</List.Content>
                                </List.Item>
                                <List.Item>
                                  <List.Icon color="red" name="marker" />
                                  <List.Content> chưa học</List.Content>
                                </List.Item>
                              </List>
                            </Segment>
                          </Grid.Column>
                          <Grid.Column width={8}>
                            <Segment vertical textAlign="left">
                              <PieChart
                                size={150}
                                labels
                                styles={{
                                  '.chart_lines': {
                                    strokeWidth: 0,
                                  },
                                  '.chart_text': {
                                    fontFamily: 'serif',
                                    fontSize: '1.25em',
                                    fill: '#333',
                                  },
                                }}
                                data={[
                                  { key: '10%', value: 10, color: 'green' },
                                  { key: '20%', value: 20, color: 'orange' },
                                  { key: '70%', value: 70, color: 'red' },
                                ]}
                              />
                            </Segment>
                          </Grid.Column>
                          <Grid.Column width={16}>
                            <Button primary circular fluid>
                              hoc tiep
                            </Button>
                          </Grid.Column>
                        </Grid>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </Segment>
              </Grid.Column>
              <Grid.Column width={5}>
                <CardExampleCard />
              </Grid.Column>
              <Grid.Column width={1} />
            </Grid.Row>
          </Grid>
        </Segment>
      </ResponsiveContainer>
    );
  }
}

export default HomepageLayout;
