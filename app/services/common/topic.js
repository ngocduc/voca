import { request } from './helper';
import { ROOT } from './config';
export async function getTopic(id) {
  // {{host}}/api/topic/topicByCategory?category_id=1
  const url = `${ROOT}/api/topic/topicByCategory?category_id=${id}`;
  try {
    const res = await request.get(url);
    return res.data.data;
  } catch (err) {
    return err;
  }
}
