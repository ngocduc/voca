import axios from 'axios';
import { getToken } from './auth';

const client = (token = null) => {
  const defaultOptions = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  return {
    get: (url, options = {}) =>
      axios.get(url, {
        headers: {
          ...defaultOptions.headers,
        },
        ...options,
      }),
    post: (url, data, options = {}) =>
      axios.post(url, data, { ...defaultOptions, ...options }),
    put: (url, data, options = {}) =>
      axios.put(url, data, { ...defaultOptions, ...options }),
    delete: (url, options = {}) =>
      axios.delete(url, { ...defaultOptions, ...options }),
  };
};

let request = client(getToken()); // eslint-disable-line

const setNewToken = newToken => {
  request = client(newToken);
};

export { request, setNewToken };
