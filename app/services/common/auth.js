import axios from 'axios';
import jwt from 'jwt-decode';
import { ROOT, TOKEN_KEY } from './config';
/* eslint-disable */
export function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
const faceToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
export async function login(email, password) {
  try {
    if (process.env.NODE_ENV === 'dnd') return faceToken;
    const response = await axios.post(`${ROOT}/api/login`, {
      email,
      password,
    });
    console.log(response)
    return response.data.data.result;
  } catch (err) {
    console.log(err)
    throw err.response.status;
  }
}

export function setToken(token) {
  if (token) localStorage.setItem(TOKEN_KEY, token);
  else localStorage.removeItem(TOKEN_KEY);
}

export const getToken = () => localStorage.getItem(TOKEN_KEY);

export function getUser() {
  const token = getToken();
  try {
    const decoded = jwt(token);
    if (!Date.now) {
      Date.now = () => new Date().getTime();
    }
    return decoded;
  } catch (error) {
    return false;
  }
}

export function isLoggedIn() {
  return !!(getUser());
}

// Useful function for working in Devtool Console
