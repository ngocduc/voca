/*
 * flashcard
 */
import React from 'react';
import ReactCardFlipFlip from 'react-card-flip';
import PropTypes from 'prop-types';
import Card from './card';
import AnswerBox from './answerBox';

class CardFlip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isFlipped: false,
    };
    this.handlePlip = this.handlePlip.bind(this);
  }

  handlePlip(e) {
    e.preventDefault();
    this.setState(prevState => ({ isFlipped: !prevState.isFlipped }));
  }

  render() {
    const { meansVN, spell, definition, example, meansEN, imgUrl } = this.props;
    return (
      <div>
        <div style={{ height: '200px' }}>
          <ReactCardFlipFlip isFlipped={this.state.isFlipped} infinite={false}>
            <Card
              key="front"
              means={meansVN}
              spell={spell}
              description={definition}
              imgUrl={imgUrl}
              handlePlip={this.handlePlip}
            />
            <Card
              key="back"
              means={meansEN}
              spell={spell}
              description={example}
              imgUrl={imgUrl}
              handlePlip={this.handlePlip}
            />
          </ReactCardFlipFlip>
        </div>
        <AnswerBox trueValue={meansEN} handlePlip={this.handlePlip} />
      </div>
    );
  }
}

CardFlip.propTypes = {
  meansVN: PropTypes.string,
  spell: PropTypes.string,
  definition: PropTypes.string,
  example: PropTypes.string,
  meansEN: PropTypes.string,
  imgUrl: PropTypes.string,
};

export default CardFlip;
