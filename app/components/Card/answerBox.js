/*
 * flashcard
 */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { get } from 'lodash';

const color = {
  div: {
    correct: '#7ac60c',
    wrong: '#ff5a4d',
  },
  button: {
    correct: '#8db750',
    wrong: '#e67d75',
  },
};
const AnswerDiv = styled.div`
  height: 100px;
  width: 100%;
  margin-top: 20px;
  background: ${props => get(color, `div.${props.answer}`, '#fff')};
  border-radius: 5px;
  box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.2);
  padding: 20px 70px 20px 30px;
  .inputCheck {
    float: left;
    width: 60%;
  }
  input {
    height: 40px;
    border: 1px solid #b9b8b8;
    border-radius: 5%;
    padding: 8px 10px;
    width: 60%;
    background-color: #fff;
    &:hover {
      border: 1px solid #a9da94;
    }
  }
`;
const Button = styled.button`
  height: 41px;
  width: 25%;
  background: ${props => get(color, `button.${props.answer}`, '#7ac60c')};
  color: #fff;
  font-size: 15px;
  border-radius: 5px;
  margin-top: 5px;
  margin-left: 20px;
`;
const NotieBox = styled.div`
  text-align: center;
  float: left;
  color: #fff;
  width: 40%;
  margin-top: -10px;
  p {
    margin: 0px;
    span {
      font-size: 26px;
      font-weight: 900;
    }
  }
  p:last-child {
    font-size: 15px;
    margin-top: 6px;
  }
  i:first {
    font-size: 30px;
    font-weight: 900;
    margin-left: 10px;
    border: 2px solid #fff;
    border-radius: 56%;
  }
`;

const NotieObj = {
  icon: {
    correct: 'done',
    wrong: 'clear',
  },
  text: {
    correct: 'correct',
    wrong: 'incorrect',
  },
};
const Notie = props => (
  <NotieBox>
    <p>
      <span>you are {get(NotieObj, `text.${props.answer}`)}</span>
      <i className="material-icons">{get(NotieObj, `icon.${props.answer}`)}</i>
    </p>
    <p>Xoay Flashcard để xem đáp án</p>
  </NotieBox>
);
Notie.propTypes = {
  answer: PropTypes.string,
};

class AnswerBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      answer: '',
    };
    this.handleChangeText = this.handleChangeText.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleOnKeyPress = this.handleOnKeyPress.bind(this);
  }

  handleChangeText(e) {
    this.setState({ text: e.target.value, answer: '' });
  }

  handleOnKeyPress(e) {
    if (e.charCode === 13) {
      this.handleCheck(e);
    }
  }

  handleCheck() {
    if (this.props.trueValue.toLowerCase() === this.state.text.toLowerCase()) {
      this.setState({ answer: 'correct' });
    } else {
      this.setState({ answer: 'wrong' });
    }
  }

  render() {
    const { answer } = this.state;
    return (
      <AnswerDiv answer={answer}>
        <div className="inputCheck">
          <input
            type="text"
            name="answer"
            placeholder="go tu vung"
            value={this.state.text}
            onChange={this.handleChangeText}
            onKeyPress={this.handleOnKeyPress}
          />
          <Button answer={answer} onClick={this.handleCheck}>
            Kiểm tra
          </Button>
        </div>
        {this.state.answer !== '' ? <Notie answer={answer} /> : null}
      </AnswerDiv>
    );
  }
}

AnswerBox.propTypes = {
  trueValue: PropTypes.string,
};

export default AnswerBox;
