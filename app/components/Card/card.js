/*
 * flashcard
 */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
const Button = styled.button``;

const Div = styled.div`
  position: absolute;
  height: 200px;
  width: 100%;
  background: #fff;
  border-radius: 5px;
  box-shadow: 0px 0px 15px 0px rgba(0, 0, 0, 0.2);
  padding: 20px 70px 20px 30px;
  .img {
    float: left;
    img {
      height: 140px;
      width: 140px;
    }
    .spell {
      margin-top: 2px;
      .icon-volume {
        top: 5px;
        position: relative;
      }
    }
  }
  .content {
    height: 160px;
    width: 60%;
    margin-top: 0px;
    margin-left: 30px;
    float: left;
    .text {
      margin: 10px 0;
    }
    .description {
      margin: 10px 0;
    }
    hr {
      width: 467px;
      border: 0;
      width: 100%;
      height: 1px;
      background: #333;
    }
  }
  .flip {
    position: absolute;
    bottom: 10px;
    right: 10px;
    color: #00bcef;
    i {
      font-size: 40px;
    }
    p {
      margin-top: 4px;
    }
  }
`;
class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      volume: 'volume_up',
    };
    this.handleVolume = this.handleVolume.bind(this);
  }

  handleVolume() {
    this.setState(
      prevState =>
        prevState.volume === 'volume_up'
          ? { volume: 'volume_off' }
          : { volume: 'volume_up' },
    );
  }

  render() {
    const { volume } = this.state;
    const { means, spell, description, key, imgUrl } = this.props;
    return (
      <Div key={key}>
        <div className="img">
          <Button>
            <img alt="img" src={imgUrl} />
          </Button>
          <div className="spell">
            <Button onClick={this.handleVolume} className="icon-volume">
              <i className="material-icons">{volume}</i>
            </Button>
            <span>{spell}</span>
          </div>
        </div>
        <div className="content">
          <div className="text">
            <span>{means}</span>
          </div>
          <hr />
          <div className="description">
            <span>{description}</span>
          </div>
        </div>
        <div className="flip">
          <Button onClick={this.props.handlePlip} className="icon-volume">
            <i className="material-icons">cached</i>
            <p>Xoay flashcard</p>
          </Button>
        </div>
      </Div>
    );
  }
}

Card.propTypes = {
  means: PropTypes.string,
  spell: PropTypes.string,
  description: PropTypes.string,
  key: PropTypes.string,
  imgUrl: PropTypes.string,
  handlePlip: PropTypes.func,
};

export default Card;
