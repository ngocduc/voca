import PropTypes from 'prop-types';
import React from 'react';
import { Redirect } from 'react-router-dom';
import {
  Button,
  Container,
  Icon,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react';
import ModalExampleCloseIcon from 'components/Popup';
import { isLoggedIn, setToken } from 'services/common/auth';

import Login from '../../containers/Login';
class DesktopContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { visible: false };
    this.logOut = this.logOut.bind(this);
  }

  componentWillMount() {
    if (isLoggedIn()) {
      this.setState({
        logOut: true,
      });
    }
  }

  handleClickSibar = () => this.setState(pre => ({ visible: !pre.visible }));

  handleSidebarHide = () => this.setState({ visible: false });

  showPopup = name => this.setState({ showPopup: name });

  handleItemClick = name => this.setState({ activeItem: name });

  closePopup = () => {
    this.setState({ showPopup: false });
  };

  hideFixedMenu = () => this.setState({ fixed: false });

  showFixedMenu = () => this.setState({ fixed: true });

  logOut() {
    setToken();
    this.setState({ logOut: false });
  }

  render() {
    if (!isLoggedIn() && !/index/.test(window.location.href))
      return <Redirect to="/index" />;
    const { children } = this.props;
    const { fixed, visible, activeItem, showPopup, logOut } = this.state;
    return (
      <Responsive
        minWidth={Responsive.onlyTablet.minWidth}
        style={{ height: '100%' }}
      >
        <ModalExampleCloseIcon
          closePopup={this.closePopup}
          showPopup={!!showPopup}
        >
          <Login type={showPopup} />
        </ModalExampleCloseIcon>
        <Sidebar.Pushable as={Segment}>
          <Sidebar
            as={Menu}
            animation="overlay"
            icon="labeled"
            inverted
            onHide={this.handleSidebarHide}
            vertical
            visible={visible}
            width="wide"
          >
            <Menu.Item>
              <Menu.Header>Products</Menu.Header>

              <Menu.Menu>
                <Menu.Item
                  name="enterprise"
                  active={activeItem === 'enterprise'}
                  onClick={this.handleItemClick}
                />
                <Menu.Item
                  name="consumer"
                  active={activeItem === 'consumer'}
                  onClick={this.handleItemClick}
                />
              </Menu.Menu>
            </Menu.Item>

            <Menu.Item>
              <Menu.Header>CMS Solutions</Menu.Header>

              <Menu.Menu>
                <Menu.Item
                  name="rails"
                  active={activeItem === 'rails'}
                  onClick={this.handleItemClick}
                />
                <Menu.Item
                  name="python"
                  active={activeItem === 'python'}
                  onClick={this.handleItemClick}
                />
                <Menu.Item
                  name="php"
                  active={activeItem === 'php'}
                  onClick={this.handleItemClick}
                />
              </Menu.Menu>
            </Menu.Item>
          </Sidebar>

          <Sidebar.Pusher dimmed={visible}>
            <Visibility
              once={false}
              onBottomPassed={this.showFixedMenu}
              onBottomPassedReverse={this.hideFixedMenu}
            >
              <Segment
                inverted
                textAlign="center"
                style={{ minHeight: 90, padding: '1em 0em' }}
                vertical
              >
                <Menu
                  fixed={fixed ? 'top' : null}
                  inverted={!fixed}
                  pointing={!fixed}
                  secondary={!fixed}
                  size="large"
                >
                  <Container>
                    <Menu.Item as="a" active onClick={this.handleClickSibar}>
                      <Icon name="sidebar" /> Menu
                    </Menu.Item>
                    <Menu.Item as="a">Work</Menu.Item>
                    <Menu.Item as="a">Company</Menu.Item>
                    <Menu.Item as="a">Careers</Menu.Item>
                    {logOut ? (
                      <Menu.Item position="right">
                        <Button onClick={this.logOut} as="a" inverted={!fixed}>
                          logOut
                        </Button>
                      </Menu.Item>
                    ) : (
                      <Menu.Item position="right">
                        <Button
                          onClick={e => this.showPopup('login', e)}
                          as="a"
                          inverted={!fixed}
                        >
                          Log in
                        </Button>
                        <Button
                          onClick={e => this.showPopup('sign-in', e)}
                          as="a"
                          inverted={!fixed}
                          primary={fixed}
                          style={{ marginLeft: '0.5em' }}
                        >
                          Sign Up
                        </Button>
                      </Menu.Item>
                    )}
                  </Container>
                </Menu>
                {/* <HomepageHeading /> */}
              </Segment>
            </Visibility>

            {children}
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </Responsive>
    );
  }
}
DesktopContainer.propTypes = {
  children: PropTypes.node,
};

export default DesktopContainer;
