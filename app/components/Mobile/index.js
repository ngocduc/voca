import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ModalExampleCloseIcon from 'components/Popup';
import Login from 'containers/Login';

import {
  Button,
  Container,
  Icon,
  Menu,
  Responsive,
  Segment,
  Sidebar,
  Header,
} from 'semantic-ui-react';

const HomepageHeading = ({ mobile }) => (
  <Container text>
    <Header
      as="h1"
      content="Imagine-a-Company"
      inverted
      style={{
        fontSize: mobile ? '2em' : '4em',
        fontWeight: 'normal',
        marginBottom: 0,
        marginTop: mobile ? '1.5em' : '3em',
      }}
    />
    <Header
      as="h2"
      content="Do whatever you want when you want to."
      inverted
      style={{
        fontSize: mobile ? '1.5em' : '1.7em',
        fontWeight: 'normal',
        marginTop: mobile ? '0.5em' : '1.5em',
      }}
    />
    <Button primary size="huge">
      Get Started
      <Icon name="right arrow" />
    </Button>
  </Container>
);

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
};
class MobileContainer extends Component {
  state = {};

  handleSidebarHide = () => this.setState({ sidebarOpened: false });

  handleToggle = () => this.setState({ sidebarOpened: true });

  showPopup = name => this.setState({ showPopup: name });

  closePopup = () => {
    this.setState({ showPopup: false });
  };

  render() {
    const { children } = this.props;
    const { sidebarOpened, showPopup } = this.state;

    return (
      <Responsive
        as={Sidebar.Pushable}
        maxWidth={Responsive.onlyMobile.maxWidth}
      >
        <ModalExampleCloseIcon
          closePopup={this.closePopup}
          showPopup={!!showPopup}
          mobile
        >
          <Login
            showPopup={this.showPopup}
            closePopup={this.closePopup}
            login={showPopup === 'login'}
          />
        </ModalExampleCloseIcon>
        <Sidebar.Pushable as={Segment}>
          <Sidebar
            as={Menu}
            animation="overlay"
            inverted
            onHide={this.handleSidebarHide}
            vertical
            visible={sidebarOpened}
            width="thin"
          >
            <Menu.Item>
              <Menu.Header>Products</Menu.Header>

              <Menu.Menu>
                <Menu.Item name="enterprise" onClick={this.handleItemClick} />
                <Menu.Item name="consumer" onClick={this.handleItemClick} />
              </Menu.Menu>
            </Menu.Item>

            <Menu.Item>
              <Menu.Header>CMS Solutions</Menu.Header>

              <Menu.Menu>
                <Menu.Item name="rails" onClick={this.handleItemClick} />
                <Menu.Item name="python" onClick={this.handleItemClick} />
                <Menu.Item name="php" onClick={this.handleItemClick} />
              </Menu.Menu>
            </Menu.Item>
          </Sidebar>

          <Sidebar.Pusher dimmed={sidebarOpened}>
            <Segment
              inverted
              textAlign="center"
              style={{ minHeight: 350, padding: '1em 0em' }}
              vertical
            >
              <Container>
                <Menu inverted pointing secondary size="large">
                  <Menu.Item onClick={this.handleToggle}>
                    <Icon name="sidebar" />
                  </Menu.Item>
                  <Menu.Item position="right">
                    <Button
                      as="a"
                      inverted
                      onClick={e => this.showPopup('login', e)}
                    >
                      Log in
                    </Button>
                    <Button
                      onClick={e => this.showPopup('sign-in', e)}
                      as="a"
                      inverted
                      style={{ marginLeft: '0.5em' }}
                    >
                      Sign Up
                    </Button>
                  </Menu.Item>
                </Menu>
              </Container>
              {/* <HomepageHeading mobile /> */}
            </Segment>

            {children}
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </Responsive>
    );
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node,
};

export default MobileContainer;
