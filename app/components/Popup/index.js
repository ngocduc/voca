import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Modal, TransitionablePortal } from 'semantic-ui-react';
class ModalExampleCloseIcon extends Component {
  state = { show: false };

  componentWillReceiveProps(nextProp) {
    this.setState({ show: nextProp.showPopup });
  }

  close = () => {
    this.setState({ show: false });
    this.props.closePopup();
  };

  open = () => this.setState({ show: this.props.showPopup });

  render() {
    const { show } = this.state;
    return (
      <TransitionablePortal
        open={show}
        onClose={this.close}
        onOpen={this.open}
        transition={{ animation: 'fly down', duration: 900 }}
      >
        <Modal
          open
          onClose={this.close}
          onOpen={this.open}
          style={{
            width: this.props.mobile ? '100%' : '40%',
          }}
          closeIcon
        >
          <Modal.Content>{this.props.children}</Modal.Content>
        </Modal>
      </TransitionablePortal>
    );
  }
}

ModalExampleCloseIcon.propTypes = {
  showPopup: PropTypes.bool,
  children: PropTypes.node,
  closePopup: PropTypes.any,
  mobile: PropTypes.any,
};
export default ModalExampleCloseIcon;
